# An API for access to abstract syntax trees

[![pipeline status](https://gitlab.com/monochromata-de/ast-api/badges/main/pipeline.svg)](https://gitlab.com/monochromata-de/ast-api/commits/main)

## Links

The project is used by
[de.monochromata.anaphors](https://gitlab.com/monochromata-de/de.monochromata.anaphors/).

## License

MIT
