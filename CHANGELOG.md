# Release 0.0.145

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.3.10

# Release 0.0.144

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.3.9

# Release 0.0.143

* chore(deps): update dependency nl.jqno.equalsverifier:equalsverifier to v3.14.2

# Release 0.0.142

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.3.8

# Release 0.0.141

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.3.7
* fix(deps): update dependency de.monochromata.poms:maven-artifact to v1.1.143

# Release 0.0.140

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.3.6

# Release 0.0.139

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.3.5

# Release 0.0.138

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.3.4

# Release 0.0.137

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.3.3

# Release 0.0.136

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.3.2

# Release 0.0.135

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.3.1

# Release 0.0.134

* fix(deps): update dependency de.monochromata.poms:maven-artifact to v1.1.142

# Release 0.0.133

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.3.0

# Release 0.0.132

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.67

# Release 0.0.131

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.65
* fix(deps): update dependency de.monochromata.poms:maven-artifact to v1.1.141

# Release 0.0.130

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.64
* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.63

# Release 0.0.129

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.62

# Release 0.0.128

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.61

# Release 0.0.127

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.60

# Release 0.0.126

* chore(deps): update junit5 monorepo to v5.9.3

# Release 0.0.125

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.59

# Release 0.0.124

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.58

# Release 0.0.123

* fix(deps): update dependency de.monochromata.poms:maven-artifact to v1.1.140

# Release 0.0.122

* chore(deps): update dependency org.mockito:mockito-core to v5.3.1

# Release 0.0.121

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.56

# Release 0.0.120

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.55

# Release 0.0.119

* chore(deps): update dependency ch.qos.logback:logback-classic to v1.4.7

# Release 0.0.118

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.53

# Release 0.0.117

* fix(deps): update dependency de.monochromata.poms:maven-artifact to v1.1.139

# Release 0.0.116

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.52

# Release 0.0.115

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.51

# Release 0.0.114

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.50

# Release 0.0.113

* fix(deps): update dependency de.monochromata.poms:maven-artifact to v1.1.138

# Release 0.0.112

* chore(deps): update dependency org.mockito:mockito-core to v5.3.0

# Release 0.0.111

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.48

# Release 0.0.110

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.47

# Release 0.0.109

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.46

# Release 0.0.108

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.45

# Release 0.0.107

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.44

# Release 0.0.106

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.41
* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.40

# Release 0.0.105

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.39

# Release 0.0.104

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.38

# Release 0.0.103

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.37
* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.36
* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.35

# Release 0.0.102

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.34

# Release 0.0.101

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.33

# Release 0.0.100

* fix(deps): update dependency de.monochromata.poms:maven-artifact to v1.1.137

# Release 0.0.99

* chore(deps): update dependency nl.jqno.equalsverifier:equalsverifier to v3.14.1

# Release 0.0.98

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.32

# Release 0.0.97

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.31

# Release 0.0.96

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.30

# Release 0.0.95

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.29

# Release 0.0.94

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.28

# Release 0.0.93

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.27

# Release 0.0.92

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.26

# Release 0.0.91

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.25

# Release 0.0.90

* chore(deps): update dependency ch.qos.logback:logback-classic to v1.4.6

# Release 0.0.89

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.23

# Release 0.0.88

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.22

# Release 0.0.87

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.21

# Release 0.0.86

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.20

# Release 0.0.85

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.19

# Release 0.0.84

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.18

# Release 0.0.83

* fix(deps): update dependency de.monochromata.poms:maven-artifact to v1.1.134

# Release 0.0.82

* chore(deps): update dependency org.mockito:mockito-core to v5.2.0
* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.17

# Release 0.0.81

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.16

# Release 0.0.80

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.15

# Release 0.0.79

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.14

# Release 0.0.78

* fix(deps): update dependency de.monochromata.poms:maven-artifact to v1.1.133

# Release 0.0.77

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.13

# Release 0.0.76

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.12

# Release 0.0.75

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.11

# Release 0.0.74

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.10

# Release 0.0.73

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.9

# Release 0.0.72

* chore(deps): update dependency nl.jqno.equalsverifier:equalsverifier to v3.14
* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.8
* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.7
* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.6
* fix(deps): update dependency de.monochromata.poms:maven-artifact to v1.1.131
* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.5
* chore(deps): update dependency nl.jqno.equalsverifier:equalsverifier to v3.13.2

# Release 0.0.71

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.2

# Release 0.0.70

* fix(deps): update dependency de.monochromata.poms:maven-artifact to v1.1.130
* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.1

# Release 0.0.69

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.2.0

# Release 0.0.68

* chore(deps): update dependency nl.jqno.equalsverifier:equalsverifier to v3.13.1

# Release 0.0.67

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.1.9

# Release 0.0.66

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.1.8

# Release 0.0.65

* chore(deps): update dependency de.monochromata.contract:java-contract to v18.1.4

# Release 0.0.64

* chore(deps): update dependency de.monochromata.contract:java-contract to v18

# Release 0.0.63

* fix(deps): update dependency de.monochromata.poms:maven-artifact to v1.1.129
* chore(deps): update dependency de.monochromata.contract:java-contract to v17.7.4

# Release 0.0.62

* chore(deps): update dependency nl.jqno.equalsverifier:equalsverifier to v3.13
* chore(deps): update dependency de.monochromata.contract:java-contract to v17.7.3

# Release 0.0.61

* chore(deps): update dependency org.mockito:mockito-core to v5.1.1

# Release 0.0.60

* fix(deps): update dependency de.monochromata.poms:maven-artifact to v1.1.128

# Release 0.0.59

* chore(deps): update dependency de.monochromata.contract:java-contract to v17.7.2

# Release 0.0.58

* fix(deps): update dependency de.monochromata.poms:maven-artifact to v1.1.127

# Release 0.0.57

* chore(deps): update dependency de.monochromata.contract:java-contract to v17.7.1

# Release 0.0.56

* chore(deps): update dependency nl.jqno.equalsverifier:equalsverifier to v3.12.4

# Release 0.0.55

* chore(deps): update dependency de.monochromata.contract:java-contract to v17.7.0

# Release 0.0.54

* chore(deps): update dependency org.mockito:mockito-core to v5

# Release 0.0.53

* fix(deps): update dependency de.monochromata.poms:maven-artifact to v1.1.125

# Release 0.0.52

* fix(deps): update dependency de.monochromata.poms:maven-artifact to v1.1.124

# Release 0.0.51

* fix(deps): update dependency de.monochromata.poms:maven-artifact to v1.1.123

# Release 0.0.50

* chore(deps): update dependency org.assertj:assertj-core to v3.24.2

# Release 0.0.49

* chore(deps): update junit5 monorepo to v5.9.2

# Release 0.0.48

* chore(deps): update dependency de.monochromata.contract:java-contract to v17.5.24

# Release 0.0.47

* chore(deps): update dependency de.monochromata.contract:java-contract to v17.5.21

# Release 0.0.46

* chore(deps): update dependency de.monochromata.contract:java-contract to v17.5.20

# Release 0.0.45

* chore(deps): update dependency de.monochromata.contract:java-contract to v17.5.19
* chore(deps): update dependency de.monochromata.contract:java-contract to v17.5.18

# Release 0.0.44

* chore(deps): update dependency de.monochromata.contract:java-contract to v17.5.17

# Release 0.0.43

* fix(deps): update dependency de.monochromata.poms:maven-artifact to v1.1.122

# Release 0.0.42

* chore(deps): update dependency org.assertj:assertj-core to v3.24.1

# Release 0.0.41

* chore(deps): update dependency de.monochromata.contract:java-contract to v17.5.16

# Release 0.0.40

* chore(deps): update dependency org.assertj:assertj-core to v3.24.0

# Release 0.0.39

* fix(deps): update dependency de.monochromata.poms:maven-artifact to v1.1.121

# Release 0.0.38

* chore(deps): update dependency de.monochromata.contract:java-contract to v17.5.15

# Release 0.0.37

* chore(deps): update dependency de.monochromata.contract:java-contract to v17.5.14

# Release 0.0.36

* chore(deps): update dependency de.monochromata.contract:java-contract to v17.5.13

# Release 0.0.35

* chore(deps): update dependency de.monochromata.contract:java-contract to v17.5.11

# Release 0.0.34

* chore(deps): update dependency de.monochromata.contract:java-contract to v17.5.10

# Release 0.0.33

* fix(deps): update dependency de.monochromata.poms:maven-artifact to v1.1.120

# Release 0.0.32

* chore(deps): update dependency org.mockito:mockito-core to v4.11.0

# Release 0.0.31

* fix(deps): update dependency de.monochromata.poms:maven-artifact to v1.1.119

# Release 0.0.30

* chore(deps): update dependency de.monochromata.contract:java-contract to v17.5.8

# Release 0.0.29

* chore(deps): update dependency de.monochromata.contract:java-contract to v17.5.7

# Release 0.0.28

* chore(deps): update dependency de.monochromata.contract:java-contract to v17.5.6

# Release 0.0.27

* fix(deps): update dependency de.monochromata.poms:maven-artifact to v1.1.118

# Release 0.0.26

* chore(deps): update dependency org.mockito:mockito-core to v4.10.0

# Release 0.0.25

* fix(deps): update dependency de.monochromata.poms:maven-artifact to v1.1.117

# Release 0.0.24

* chore(deps): update dependency nl.jqno.equalsverifier:equalsverifier to v3.12.3

# Release 0.0.23

* fix(deps): update dependency de.monochromata.poms:maven-artifact to v1.1.116

# Release 0.0.22

* chore(deps): update dependency de.monochromata.contract:java-contract to v17

# Release 0.0.21

* chore(deps): update junit5 monorepo to v5.9.1

# Release 0.0.20

* chore(deps): update dependency org.mockito:mockito-core to v4.9.0

# Release 0.0.19

* chore(deps): update dependency org.assertj:assertj-core to v3.23.1

# Release 0.0.18

* chore(deps): update dependency nl.jqno.equalsverifier:equalsverifier to v3.12.2

# Release 0.0.17

* chore(deps): update dependency de.monochromata.contract:java-contract to v15.3.0

# Release 0.0.16

* fix(deps): update dependency de.monochromata.poms:maven-artifact to v1.1.115

# Release 0.0.15

* fix(deps): update dependency de.monochromata.poms:maven-artifact to v1.1.114

# Release 0.0.14

* fix(deps): update dependency de.monochromata.poms:maven-artifact to v1.1.113

# Release 0.0.13

* fix(deps): update dependency de.monochromata.poms:maven-artifact to v1.1.112

# Release 0.0.12

* fix(deps): update dependency de.monochromata.poms:maven-artifact to v1.1.111

# Release 0.0.11

* fix(deps): update dependency de.monochromata.poms:maven-artifact to v1.1.110

# Release 0.0.10

* fix(deps): update dependency de.monochromata.poms:maven-artifact to v1.1.109

# Release 0.0.9

* fix(deps): update dependency de.monochromata.poms:maven-artifact to v1.1.108

# Release 0.0.8

* fix(deps): update dependency de.monochromata.poms:maven-artifact to v1.1.107

# Release 0.0.7

* fix(deps): update dependency de.monochromata.poms:maven-artifact to v1.1.106

# Release 0.0.6

* fix(deps): update dependency de.monochromata.poms:maven-artifact to v1.1.105

# Release 0.0.5

* fix(deps): update dependency de.monochromata.poms:maven-artifact to v1.1.104

# Release 0.0.4

* chore(deps): update dependency ch.qos.logback:logback-classic to v1.4.5
* Fit declarations to constructor argument order
* Correct pipeline status badge

# Release 0.0.3

* Manually update version to 0.0.2
* Update build-tools branch to main
* chore(deps): update dependency org.mockito:mockito-core to v4
* chore(deps): update dependency nl.jqno.equalsverifier:equalsverifier to v3.9
* chore(deps): update junit5 monorepo to v5.8.2
* chore(deps): update dependency de.monochromata.contract:java-contract to v15.0.70
* chore(deps): update dependency ch.qos.logback:logback-classic to v1.2.10
* fix: JavaDoc errors
* Correct name and version in POM
* feat: Initial commit

