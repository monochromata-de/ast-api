package de.monochromata.ast;

public enum RelatedExpressionType {

    CLASS_INSTANCE_CREATION, LOCAL_VARIABLE_DECLARATION, METHOD_INVOCATION, PARAMETER_DECLARATION

}
