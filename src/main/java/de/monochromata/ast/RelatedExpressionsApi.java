package de.monochromata.ast;

import java.util.Set;

/**
 * A special service provider interface to perform actions related to finding
 * and processing related expressions.
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type The class instance creation expression node
 *             type
 * @param <T>  The type type
 * @param <B>  The binding type
 * @param <MB> The method binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <EV> The type of the event contained in the condition that is
 *             evaluated to check when the perspectivations shall be applied.
 * @param <PP> The type used for positions that carry perspectivations
 */
public interface RelatedExpressionsApi<N, E, T, B, MB extends B, TB extends B, S, I, QI, EV, PP> {

    /**
     * @return {@literal true} if the {@code node1} is able to declare more than one
     *         variable but {@code node2} is the only variable declaration that
     *         {@code node1} actually contains.
     */
    boolean isOnlyFragmentOfMultiVariable(final N node1, final N node2);

    boolean hasInitializer(N node, N potentialInitializer);

    T getReservedTypeVar(S scope);

    // TODO: Array creation

    Set<QI> getFeatures(N relatedExpression);

    /**
     * Compares all strings occurring in the two AST nodes.
     *
     * @throws IllegalArgumentException if the given nodes do not have the same
     *                                  type.
     */
    boolean compare(N node1, N node2);

    String getDescription(N node);

    int getLine(N node);

    int getColumn(N node);

    I getIdentifier(final TB typeBinding);

    int getLengthOfSimpleNameOfType(final T type);

    int getLength(final I identifier);

    /*
     * TODO: Move logic from PublicDom to d.m.a to remove d.m.a types from the API
     * default I guessTempName(final R relatedExpression, final String anaphor,
     * final LocalTempVariableContents localTempVariableContents, final S scope) {
     * return guessTempName(relatedExpression, singletonList(new
     * ImmutablePair<>(localTempVariableContents, anaphor)), scope); }
     *
     * I guessTempName(final R relatedExpression, final
     * List<Pair<LocalTempVariableContents, String>> indirectionsAndAnaphors, final
     * S scope);
     */

    QI toQualifiedIdentifier(final I identifier);

    MB resolveMethodInvocationBinding(N relatedExpression, S scope);

    String identifierToString(final I identifier);

    String qualifiedIdentifierToString(final QI qualifiedIdentifier);

    default boolean supportsLocalVariableTypeInference(final S scope) {
        return false;
    }

    /*
     * TODO: Move logic from PublicDom to d.m.a to remove d.m.a types from the API
     * PP createPositionForNode(final N node, final Predicate<EV> condition, final
     * List<Perspectivation> perspectivations);
     */
}
