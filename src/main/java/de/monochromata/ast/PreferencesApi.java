package de.monochromata.ast;

public interface PreferencesApi {

    boolean getAddFinalModifierToCreatedTemporaryVariables();

    boolean getUseLocalVariableTypeInference();

}
