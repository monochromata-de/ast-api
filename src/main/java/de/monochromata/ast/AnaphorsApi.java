package de.monochromata.ast;

import java.util.Set;

/**
 * The service provider interface used by the
 * <code>de.monochromata.anaphors</code> package to perform anaphora resolution
 * on a given AST implementation.
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <EV> The type of the event contained in the condition that is
 *             evaluated to check when the perspectivations shall be applied.
 * @param <PP> The type used for positions that carry perspectivations
 */
public interface AnaphorsApi<N, E, TB, S, I, QI, EV, PP> {

    public boolean isSimpleName(E definiteExpression);

    public I getIdentifierOfSimpleName(E simpleName);

    /*
     * TODO: Move logic from PublicDom to d.m.a to remove d.m.a types from the API
     * public boolean nameOfReferentEqualsIdentifier(Referent<TB, S, I, QI>
     * referent, I id, boolean caseSensitive);
     */

    /*
     * TODO: Move logic from PublicDom to d.m.a to remove d.m.a types from the API
     *
     * Returns true, if the name of the referent matches a suffix of the identifier.
     *
     * @param referent the referent whose name to match
     *
     * @param id the id to match
     *
     * @param caseSensitive whether case should be considered during matching
     *
     * @return {@code true}, if the name of the referent matches a suffix of the
     * identifier, {@code false} otherwise (including when the referent has no
     * name).
     *
     * @see Referent#hasName() public boolean
     * nameOfReferentMatchesConceptualTypeOfIdentifier(Referent<TB, S, I, QI>
     * referent, I id, boolean caseSensitive);
     *
     * public Set<QI>
     * getFeaturesRemainingInIdentifierBesidesConceptualTypeOfReferentName(I id,
     * Referent<TB, S, I, QI> referent, boolean caseSensitive);
     */

    /**
     * Test whether the given identifier matches the simple name of the given type
     * binding.
     *
     * TODO: Add the possibility to match prefix/suffix
     *
     * @param id            The identifier to match.
     * @param type          The typing binding whose simple name is to be matched.
     * @param caseSensitive True, if case is to be considered, false, if it is to be
     *                      ignored. TODO: What about the initial character?
     * @return True, if the identifier matches the simple name of the type binding,
     *         false otherwise.
     */
    public boolean nameOfIdentifierEqualsSimpleNameOfTypeBinding(I id, TB type, boolean caseSensitive);

    public boolean conceptualTypeInIdentifierEqualsSimpleNameOfType(I id, TB type, boolean caseSensitive);

    public Set<QI> getFeaturesRemainingInIdentifierBesidesConceptualTypeOfReferentType(I id, TB referentType,
            boolean caseSensitive);

    public boolean nameOfIdentifierEqualsFauxHyponymOfSimpleNameOfTypeBinding(I id, TB type, boolean caseSensitive);

    public boolean conceptualTypeInIdentifierEqualsFauxHyponymyOfSimpleNameOfType(I id, TB type, boolean caseSensitive);

    public Set<QI> getFeaturesRemainingInIdentifierBesidesConceptualTypeOfReferentTypeWithFauxHyponymy(I id, TB type,
            boolean caseSensitive);

    /*
     * TODO: Move logic from PublicDom to d.m.a to remove d.m.a types from the API
     * public PP createPositionForExpression(final E expression, final Predicate<EV>
     * condition, final List<Perspectivation> perspectivations);
     */

    public int getLength(final QI qualifiedIdentifier);
}
