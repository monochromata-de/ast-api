package de.monochromata.ast;

/**
 * An interface to create AST transformations.
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <T>  The type type
 * @param <B>  The binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 */
public interface TransformationsApi<N, E, T, B, TB extends B, S, I, QI> {

    /**
     * @return {@literal true} if both nodes are contained in or are the same
     *         invocable (i.e. initializer, constructor, method or lambda
     *         expression).
     */
    public boolean partOfSameInvocable(N node, E expression);

    /*
     * TODO: Move logic from PublicDom to d.m.a to remove d.m.a types from the API
     *
     * Attempts to create a transformation that pass the given upstream node or a
     * representation of it along the call chain from the invocable (i.e.
     * initializer, constructor, method or lambda expression) containing the
     * upstream node to the invocable containing the downstream node. <p> Such a
     * transformation can only be constructed if there is a call chain connecting
     * upstream and downstream nodes and if the chain is no longer than the
     * implementation-specific analysis limits of the SPI implementation.
     *
     * @return a transformation if it can be constructed, or {@literal null}
     * otherwise.
     *
     * public ASTTransformation<N, E, T, B, TB, S, I, QI, R, A> passAlongCallChain(N
     * upstreamNode, E downstreamExpression, S scope);
     */
}
