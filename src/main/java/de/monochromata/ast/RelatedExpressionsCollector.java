package de.monochromata.ast;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

/**
 * An interface to objects used to traverse the AST to find potential related
 * expressions.
 * <p>
 * Note that the implementations of this interface may support only a limited
 * set of {@link RelatedExpressionType}s.
 *
 * @param <E> The expression type
 * @param <S> The scope type (optional)
 */
public interface RelatedExpressionsCollector<E, S> {

    /**
     * Traverse the body declaration that contains the given definite expression.
     * <p>
     * TODO: Rename the method to make clear that the enclosing body declaration is
     * to be traversed?
     * <p>
     * TODO: There might be related expression that do not traverse the body
     * enclosing the given definite expression but that check whether that body is
     * reachable from some place
     *
     * @param definiteExpression The definite expression whose enclosing body is to
     *                           be traversed.
     * @param scope              The scope containing the definite expression if
     *                           used by the compiler-specific implementation.
     *                           Implementations of this method must not access the
     *                           scope but merely pass it on to SPI's they invoke.
     * @return collected potential related expressions
     */
    public List<Pair<RelatedExpressionType, E>> traverse(E definiteExpression, S scope);
}
