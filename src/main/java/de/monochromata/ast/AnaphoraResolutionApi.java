package de.monochromata.ast;

import java.util.List;

/**
 * A special service provider interface related to finding potential referents
 * of a given definite expression in a given related expression.
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <T>  The type type
 * @param <B>  The binding type
 * @param <VB> The variable binding type
 * @param <FB> The field binding type
 * @param <MB> The method binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 */
public interface AnaphoraResolutionApi<N, E, T, B, VB extends B, FB extends B, MB extends B, TB extends B, S, I, QI> {

    List<FB> getAccessibleFields(TB typeBinding, S scope);

    I getFieldName(FB fieldBinding);

    boolean identifierStartsUpperCase(I identifier);

    String getFieldDescription(FB fieldBinding);

    TB resolveType(FB fieldBinding, S scope);

    List<MB> getAccessibleGetterMethods(TB typeBinding, S scope);

    boolean doesGetterMethodOnlyReturnValueOf(MB methodBinding, FB fieldBinding, S scope);

    /**
     * Return a variable name for the given method that removes getter or setter
     * prefix to the method name and converts the first character of the remaining
     * string to lower case.
     *
     * @param methodBinding The method binding to use.
     * @return A variable name.
     */
    QI getReferentName(MB methodBinding);

    I getMethodName(MB methodBinding);

    String getMethodDescription(MB methodBinding);

    TB resolveReturnType(MB methodBinding, S scope);

    boolean couldBeAPreviousRealization(E definiteExpression);

    I getIdFromPreviousRealization(E definiteExpression);

    /**
     * TODO: Move more code from PublicAnaphoraResolution to d.m.a without adding
     * types from d.m.a to the AST API
     *
     * Replace an anaphor of kind DA1Re by an expression that realizes it.
     *
     * @param replacee        The anaphor to be replaced.
     * @param guessedTempName If the given related expression part has a local temp
     *                        variable introducing strategy, the strategy introduced
     *                        a temporary variable that might be used by the anaphor
     *                        resolution strategy to generate code for the anaphors.
     * @param support         Optional compiler-specific support objects.
     * @return The AST node that realizes the anaphor.
     *
     *         E realizeDA1Re(RelatedExpressionPart<N, E, T, B, TB, S, I, QI, R>
     *         relatedExpressionPart, AnaphorPart<N, E, T, B, TB, S, I, QI, R, A>
     *         anaphorPart, E replacee, Optional<I> guessedTempName, Object...
     *         support);
     *
     *         E realizeIA1Mr(RelatedExpressionPart<N, E, T, B, TB, S, I, QI, R>
     *         relatedExpressionPart, AnaphorPart<N, E, T, B, TB, S, I, QI, R, A>
     *         anaphorPart, E replacee, Optional<I> guessedTempName, Object[]
     *         support);
     *
     *
     *         Replace an anaphor of kind IA2F by an expression that realizes it.
     *
     * @param replacee        The anaphor to be replaced.
     * @param guessedTempName If the given related expression part has a local temp
     *                        variable introducing strategy, the strategy introduced
     *                        a temporary variable that might be used by the anaphor
     *                        resolution strategy to generate code for the anaphors.
     * @param support         Optional compiler-specific support objects.
     * @return The AST node that realizes the anaphor.
     *
     *         E realizeIA2F(RelatedExpressionPart<N, E, T, B, TB, S, I, QI, R>
     *         relatedExpressionPart, AnaphorPart<N, E, T, B, TB, S, I, QI, R, A>
     *         anaphorPart, E replacee, Optional<I> guessedTempName, Object...
     *         support);
     *
     *         Replace an anaphor of kind IA2Mg by an expression that realizes it.
     *
     * @param replacee        The anaphor to be replaced.
     * @param guessedTempName If the given related expression part has a local temp
     *                        variable introducing strategy, the strategy introduced
     *                        a temporary variable that might be used by the anaphor
     *                        resolution strategy to generate code for the anaphors.
     * @param support         Optional compiler-specific support objects.
     * @return The AST node that realizes the anaphor.
     *
     *         E realizeIA2Mg(RelatedExpressionPart<N, E, T, B, TB, S, I, QI, R>
     *         relatedExpressionPart, AnaphorPart<N, E, T, B, TB, S, I, QI, R, A>
     *         anaphorPart, E replacee, Optional<I> guessedTempName, Object...
     *         support);
     *
     *
     *         String getIA1MrUnderspecifiedRelation(R potentialRelatedExpression,
     *         MB methodBinding, S scope);
     *
     *         Returns the underspecified relation represented by an indirect
     *         anaphora relation of kind IA2F in the format
     *         <code>&lt;fully-qualified name of field declaring type&gt;.&lt;fieldName&gt;</code>
     *         .
     *
     * @param relatedExpression The related expression whose type declares the
     *                          underspecified relation.
     * @param fieldBinding      The field that is part of the underspecified
     *                          relation.
     * @param scope             The scope containing the anaphor if used by the
     *                          compiler-specific implementation.
     * @return A string representing the underspecified relation.
     *
     *         String getIA2FUnderspecifiedRelation(R relatedExpression, FB
     *         fieldBinding, S scope);
     *
     *         String getIA2MgUnderspecifiedRelation(R relatedExpression, MB
     *         methodBinding, S scope);
     */
}
