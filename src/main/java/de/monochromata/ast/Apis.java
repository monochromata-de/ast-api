package de.monochromata.ast;

import java.util.function.Supplier;

public class Apis {

    public final AnaphoraResolutionApi anaphoraResolutionApi;
    public final AnaphorsApi anaphorsApi;
    public final PreferencesApi preferencesApi;
    public final RelatedExpressionsApi relatedExpressionsApi;
    public final TransformationsApi transformationsApi;
    public final Supplier<RelatedExpressionsCollector> relatedExpressionsCollectorSupplier;

    public Apis(final AnaphoraResolutionApi anaphoraResolutionApi, final AnaphorsApi anaphorsApi,
            final PreferencesApi preferencesApi, final RelatedExpressionsApi relatedExpressionsApi,
            final TransformationsApi transformationsApi,
            final Supplier<RelatedExpressionsCollector> relatedExpressionsCollectorSupplier) {
        this.anaphoraResolutionApi = anaphoraResolutionApi;
        this.anaphorsApi = anaphorsApi;
        this.preferencesApi = preferencesApi;
        this.relatedExpressionsApi = relatedExpressionsApi;
        this.transformationsApi = transformationsApi;
        this.relatedExpressionsCollectorSupplier = relatedExpressionsCollectorSupplier;
    }

}
